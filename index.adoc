= Interface Control Document (DaC proof of concept)

This is a minimalist template of an ICD for an interface of a fictional system, using the Asciidoc markup language. As with any other markup language, this document will contain only plain text, which makes it easier to maintain over time, as it doesn't depend on proprietary editing tools. Furthermore, by being text-based, this kind of document works well with existing (and proven) software-source code management tools (e.g., version control systems, CI/CD platforms, etc). This template is expected to be 'rendered' as an HTML document (PDF and other formats could be also used as a target output) by an Asciidoc processor, which includes a number of extensions that define custom macros (special keywords that will be expanded as content) for ICDs. The documentation pipeline this document has to go through before getting published, on the other hand, will ensure it fulfills the minimum quality standards defined by the organization (e.g., regarding technical content or writing style).

== Purpose

== Versions history

tagshistory::default[]

== Reference documents

references::default[]

== Glossary


== System overview


This is a system overview described using text-based diagram specifications that makes easier its maintinance and version control. Here you can use the 'acr' macro (for example, with the acronym acr:AIPS++[]) to automatically include its description on the document's glossary, based on the centralized glossary data source defined for the tool (at the moment the building process is configured with a data-source mock, with only a few acronyms). This will be extended to also include context-based definitions and abbreviations (linked to a proper terminology management systems). 

In this document you can make use of the existing Asciidoctor plugins to use any of the text-based diagram definition languages available today. However, due to the limitations of the evaluation environment (gitlab public runners) only plantUML is enabled at the moment:


[plantuml, format=png]   
----
scale 350 width
[*] --> NotShooting

state NotShooting {
  [*] --> Idle
  Idle --> Configuring : EvConfig
  Configuring --> Idle : EvConfig
}

state Configuring {
  [*] --> NewValueSelection
  NewValueSelection --> NewValuePreview : EvNewValue
  NewValuePreview --> NewValueSelection : EvNewValueRejected
  NewValuePreview --> NewValueSelection : EvNewValueSaved

  state NewValuePreview {
     State1 -> State2
  }

}
----


== Interfacing details

*Registry maps specifications*

The following is a proof of concept of the Asciidoctor extensions that are being developed to allow a technical writer to specify a hardware interface (in this case, a registers map) following an industry standard like SystemRDL. The document building process take care of turning this specification into a readable document, and to generate the necessary artifacts to make the development of the software artifacts that require these specifications more straightforward.

SystemRDL albeit being quite complete, has most of its elements optional. In order to fullfil the expected quality of the document, the building process would allow the process to include 'quality gates' that ensure the minimum elements required for the proper understanding of the system are included. The registers specifications can be defined inline using the [systemrdl] macro. For larger registry maps, the specifications can be maintained in separate files. In both cases, the details of any systax error are informed on the CI/CD platform's dashboard.

Furthermore, it is worth noting that the platform is extensible, so further extensions for more specifications formalisms could be integrated with relative ease.






